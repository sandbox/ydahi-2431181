<?php
/**
 * @file
 * An interaction with point-based, clustered maps that allows
 * clicking on points that results in scanning between items.
 */
class Openlayers_popout_behavior_popout extends openlayers_behavior {
  /**
   * Provide initial values for options.
   */
  public function options_init() {
    return array(
      'layers' => array(),
    );
  }

  /**
   * Form defintion for per map customizations.
   */
  public function options_form($defaults = array()) {
    // Only prompt for vector layers.
    $vector_layers = array();
    foreach ($this->map['layers'] as $id => $name) {
      $layer = openlayers_layer_load($id);
      if (isset($layer->data['vector']) && $layer->data['vector'] == TRUE) {
        $vector_layers[$id] = $name;
      }
    }

    return array(
      'layers' => array(
        '#title' => t('Layers'),
        '#type' => 'checkboxes',
        '#options' => $vector_layers,
        '#description' => t('Select layer to apply popouts to.'),
        '#default_value' => isset($defaults['layers']) ? $defaults['layers'] : array(),
      ),
      
	  'targetbox' => array(
        '#title' => t('Target Container'),
        '#type' => 'textfield',
        '#title' => t('ID of the DIV element to show content in. Default: #pop-out.'),
        '#description' => t('The popout content will display in the container. Default: #pop-out.'),
        '#default_value' => isset($defaults['targetbox']) ? $defaults['targetbox'] : NULL,
      ),

      'hover' => array(
        '#title' => t('Hover'),
        '#type' => 'checkbox',
        '#description' => t('Select to display popup on mouse hover.'),
        '#default_value' => isset($defaults['hover']) ? $defaults['hover'] : NULL,
      ),
    );
  }
  

  
  /**
   * Render.
   */
  public function render(&$map) {
    drupal_add_css(drupal_get_path('module', 'openlayers_popout') . '/behaviors/openlayers_popout_behavior_popout.css');
    drupal_add_js(drupal_get_path('module', 'openlayers_popout') . '/behaviors/openlayers_popout_behavior_popout.js');
    drupal_add_js(array('popout' => $this->options), 'setting');
    return $this->options;
  }
}
